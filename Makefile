clean:
	docker image rm komz-api-comment:latest node:10-alpine sr3thore/komz-api-comment:latest
install:
	docker build -t sr3thore/komz-api-comment . && \
	docker-compose build && \
	npm install
ip:
	docker inspect -f '{{range.NetworkSettings.Networks}}{{.IPAddress}}{{end}}' komz-api-comment
logs:
	docker-compose logs -f api
start:
	docker-compose up -d
stop:
	docker-compose down
