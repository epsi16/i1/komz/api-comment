const express = require('express');
const app = express();
const cors = require('cors');
const axios = require('axios');
const fileUpload = require('express-fileupload');
const FormData = require('form-data');
const { Pool } = require('pg');
const { v4: uuidv4 } = require('uuid');
const config = require('./config');

app.use(cors());

app.use(express.json());

app.use(fileUpload());

const pool = new Pool({
    user: config.POSTGRESQL_USER,
    host: config.POSTGRESQL_HOST,
    database: config.POSTGRESQL_DATABASE,
    password: config.POSTGRESQL_PASSWORD,
    port: config.POSTGRESQL_PORT
});

app.get('/comment/bdd', (req, res) => {

    console.log("[GET] /comment/bdd");

    console.log(config.POSTGRESQL_USER);
    console.log(config.POSTGRESQL_HOST);
    console.log(config.POSTGRESQL_DATABASE);
    console.log(config.POSTGRESQL_PASSWORD);
    console.log(config.POSTGRESQL_PORT);
    console.log(config.API_USER_URL);
    console.log(config.API_IMAGE_URL);
    console.log(config.API_LIKE_URL);
    res.header('Access-Control-Allow-Origin', '*');

    console.log(pool.connect());

    res.send("ok");
});

app.get('/comment/:id', async(req, res) => {

    console.log("[GET] /comment/" + req.params.id);

    res.header('Access-Control-Allow-Origin', '*');

    pool.query('SELECT *, (SELECT count(*) FROM PUBLIC.comment WHERE id_parent = $1) AS count_child FROM PUBLIC.comment WHERE id = $1', [req.params.id], async(error, result) => {
        if (result.rows.length !== 1) {
            res.status(404).send({ error: "This comment doesn't exist" });
        }else{

            const resUser = await axios.get(config.API_USER_URL + "/" + result.rows[0].username_user);

            const resCountLikes = await axios.get(config.API_LIKE_URL + "/comment_" + result.rows[0].id);

            res.send({
                'id' : result.rows[0].id,
                'boolean_parent' : result.rows[0].boolean_parent,
                'content' : result.rows[0].content,
                'count_child': result.rows[0].count_child,
                'link_image' : result.rows[0].link_image,
                'id_parent' : result.rows[0].id_parent,
                'id_post' : result.rows[0].id_post,
                'username_user' : result.rows[0].username_user,
                'added' : result.rows[0].added,
                'updated' : result.rows[0].updated,
                'link_user_image' : (resUser !== null && resUser.data !== null) ? resUser.data.link_image_profile : null,
                'count_likes' : (resCountLikes !== null && resCountLikes.data !== null) ? resCountLikes.data : null,
            });
        }
    })
});

app.get('/comment/post/number/:id_post', async(req, res) => {

    console.log("[GET] /comment/post/number/" + req.params.id_post);

    res.header('Access-Control-Allow-Origin', '*');

    pool.query('SELECT count(*) FROM PUBLIC.comment WHERE id_post = $1', [req.params.id_post], async(error, result) => {
        res.send(result.rows[0].count);
    })
});

app.get('/comment/post/:id_post/:first/:number', async(req, res) => {

    console.log("[GET] /comment/post/" + req.params.id_post + "/" + req.params.first + "/" + req.params.number);

    res.header('Access-Control-Allow-Origin', '*');

    let queryGET = 'SELECT *, (SELECT count(*) FROM PUBLIC.comment WHERE id_parent = c.id) AS count_child FROM PUBLIC.comment c WHERE id_post = $1 AND boolean_parent = $2 ORDER BY c.added DESC LIMIT $3 OFFSET $4';
    let paramsGET = [req.params.id_post, false, req.params.number, req.params.first];

    pool.query(queryGET, paramsGET, async(error, result) => {
        if (result.rows.length < 1) {
            res.status(404).send({ error: "There is no more comments for this post" });
        }else{
            let comments = [];

            for(let row of result.rows){

                const resUser = await axios.get(config.API_USER_URL + "/" + row.username_user);

                const resCountLikes = await axios.get(config.API_LIKE_URL + "/comment_" + result.rows[0].id);

                comments.push({
                    'id': row.id,
                    'boolean_parent': row.boolean_parent,
                    'content': row.content,
                    'count_child': row.count_child,
                    'link_image': row.link_image,
                    'id_parent': row.id_parent,
                    'id_post': row.id_post,
                    'username_user': row.username_user,
                    'added': row.added,
                    'updated': row.updated,
                    'link_user_image' : (resUser !== null && resUser.data !== null) ? resUser.data.link_image_profile : null,
                    'count_likes' : (resCountLikes !== null && resCountLikes.data !== null) ? resCountLikes.data : null,
                })
            }

            res.send(comments);
        }
    })
});

app.get('/comment/child/:id_parent/:first/:number', async(req, res) => {

    console.log("[GET] /comment/child/" + req.params.id_parent + "/" + req.params.first + "/" + req.params.number);

    res.header('Access-Control-Allow-Origin', '*');

    let queryGET = 'SELECT * FROM PUBLIC.comment c WHERE id_parent = $1 ORDER BY c.added DESC LIMIT $2 OFFSET $3';
    let paramsGET = [req.params.id_parent, req.params.number, req.params.first];

    pool.query(queryGET, paramsGET, async(error, result) => {
        if (result.rows.length < 1) {
            res.status(404).send({ error: "There is no more child comments for this comment" });
        }else{
            let comments = [];

            for(let row of result.rows){

                const resUser = await axios.get(config.API_USER_URL + "/" + row.username_user);

                const resCountLikes = await axios.get(config.API_LIKE_URL + "/comment_" + result.rows[0].id);

                comments.push({
                    'id': row.id,
                    'boolean_parent': row.boolean_parent,
                    'content': row.content,
                    'count_child': row.count_child,
                    'link_image': row.link_image,
                    'id_parent': row.id_parent,
                    'id_post': row.id_post,
                    'username_user': row.username_user,
                    'added': row.added,
                    'updated': row.updated,
                    'link_user_image' : (resUser !== null && resUser.data !== null) ? resUser.data.link_image_profile : null,
                    'count_likes' : (resCountLikes !== null && resCountLikes.data !== null) ? resCountLikes.data : null,
                })
            }

            res.send(comments);
        }
    })
});


app.get('/comment/islikeby/:id_comment/:id_user', async(req, res) => {

    console.log("[GET] /comment/islikeby/" + req.params.id_comment + "/" + req.params.id_user);

    res.header('Access-Control-Allow-Origin', '*');

    const resIsLike = await axios.get(config.API_LIKE_URL + "/byUser/" + req.params.id_user + "/comment/" + req.params.id_comment);

    let isLike = (resIsLike !== null && resIsLike.data !== null) ? resIsLike.data : null;

    res.send(isLike);

});

app.post('/comment', async(req, res) => {

    console.log("[POST] /comment");

    res.header('Access-Control-Allow-Origin', '*');

    const { content, id_parent, id_post, username_user, added } = req.body;

    const id_parent_parsed = (id_parent !== 'null') ? id_parent : null;

    let uuidValid = false;
    let uuid = uuidv4();

    while (!uuidValid) {
        await new Promise((resolve, reject) => {
            pool.query('SELECT COUNT(*) FROM PUBLIC.comment WHERE id = $1', [uuid], (error, result) => {
                resolve(result);
            });
        }).then((result) => {
            uuidValid = (result.rows[0].count == 0) ? true : false;
            uuid = (uuidValid == true) ? uuid : uuidv4();
        });
    }

    let link_image;
    if(req.files !== null && typeof(req.files) !== "undefined" && req.files.image !== null){
        const { headers, files } = req;
        const { data, name } = files.image;

        const formData = new FormData();
        formData.append('name', 'comment-' + uuid)
        formData.append('content', data, name);

        const resSub = await axios.post(config.API_IMAGE_URL, formData, {
            headers: formData.getHeaders()
        });

        link_image = resSub.data.url;
    }else{
        link_image = null;
    }

    let boolean_parent = (id_parent_parsed == null) ? false : true;

    let queryPOST = 'INSERT INTO PUBLIC.comment (id, boolean_parent, content, link_image, id_parent, id_post, username_user, added, updated) VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9)';
    let paramsPOST = [uuid, boolean_parent, content, link_image, id_parent_parsed, id_post, username_user, added, added];

    pool.query(queryPOST, paramsPOST, (error, result) => {
        if (error) {
            res.status(400).send({ error: "An error has been thrown during the insert of the comment" });
        } else {
            if(result.rowCount == 0){
                res.status(404).send({ error: "The insert of the comment has failed" });
            }else{
                res.status(200).send({ message: "Comment has been created" });
            }
        }
    })
});

app.put('/comment', async(req, res) => {

    console.log("[PUT] /comment");

    res.header('Access-Control-Allow-Origin', '*');

    const { id, content, updated } = req.body;

    let idValid;

    await new Promise((resolve, reject) => {
        pool.query('SELECT * FROM PUBLIC.comment WHERE id = $1', [id], (error, result) => {
            resolve(result);
        });
    }).then((result) => {
        idValid = (result.rowCount === 1) ? true : false;
    });

    if(idValid === true){

        let link_image;
        let queryPUT;
        let paramsPUT;

        if(req.files !== null && typeof(req.files) !== "undefined" && req.files.image !== null){
            const { headers, files } = req;
            const { data, name } = files.image;

            const formData = new FormData();
            formData.append('name', 'comment-' + id)
            formData.append('content', data, name);

            const resSub = await axios.post(config.API_IMAGE_URL, formData, {
                headers: formData.getHeaders()
            });

            link_image = resSub.data.url;

            queryPUT = 'UPDATE PUBLIC.comment SET content = $1, link_image = $2, updated = $3 WHERE id = $4';
            paramsPUT = [content, link_image, updated, id];
        }else{
            queryPUT = 'UPDATE PUBLIC.comment SET content = $1, link_image = $2, updated = $3 WHERE id = $4';
            paramsPUT = [content, link_image, updated, id];
        }

        pool.query(queryPUT, paramsPUT, (error, result) => {
            if(error){
                res.status(400).send({ error: "An error has been thrown during the update of the comment" });
            }else{
                if(result.rowCount === 0){
                    res.status(404).send({ error: "The update of the comment has failed" });
                }else {
                    res.status(200).send({ message: "Comment has been updated" });
                }
            }
        })
    }else{
        res.status(400).send({ error: "An error has been thrown during the update of the comment" });
    }
});

app.delete('/comment/:id', (req, res) => {

    console.log("[DELETE] /comment/" + req.params.id);

    res.header('Access-Control-Allow-Origin', '*');

    pool.query("DELETE FROM PUBLIC.comment WHERE id = $1", [req.params.id], (error, result) => {
        if(error){
            res.status(400).send({ error: "An error has been thrown during the delete of the comment" });
        }else{
            if(result.rowCount === 0){
                res.status(404).send({ error: "The delete of the comment has failed" });
            }else {
                res.status(200).send({ message: "Comment has been deleted" });
            }
        }
    })
});

app.listen(8080, () => {
    console.log("Server listening on port 8080...");
})
