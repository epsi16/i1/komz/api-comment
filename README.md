# API Comment

## Installation
- `cp docker-compose.yml.dist docker-compose.yml`
  - Ajouter vos `POSTGRESQL_HOST`, `POSTGRESQL_USER`, `POSTGRESQL_PASSWORD`, `POSTGRESQL_DATABASE`, `POSTGRESQL_PORT`, `API_IMAGE_URL`, `API_USER_URL` et `API_LIKE_URL`
- `cp .env.dist .env`
    - Ajouter vos `POSTGRESQL_HOST`, `POSTGRESQL_USER`, `POSTGRESQL_PASSWORD`, `POSTGRESQL_DATABASE`, `POSTGRESQL_PORT`, `API_IMAGE_URL`, `API_USER_URL` et `API_LIKE_URL`
- `make install`

## Démarrage
- `make start`

## Arrêt
- `make stop`

## Supprimer le conteneur et ses images
- `make clean`

## Consulter les logs
- `make logs`

## Réccupérer l'IP du conteneur
- `make ip`
